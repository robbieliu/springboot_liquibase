package com.liuwq.job.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author dylan.qin
 * @Since: 2021-07-31 14:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("dosm_xxl_job_log")
public class XxlJobLogPo {

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID",example="123")
    @TableId("id")
    protected String id;

    /**
     * xxl任务Id
     */
    @ApiModelProperty(value = "xxlJob的任务ID",example="123")
    @TableField("job_id")
    private Long jobId;

    /**
     * 执行时间
     */
    @ApiModelProperty(value = "任务执行时间",example="2021-10-01 01:00:00")
    @TableField("handle_time")
    private Date handleTime =new Date();
    /**
     * 执行结果  0-成功 1-失败
     */
    @ApiModelProperty(value = "执行结果  0-成功 1-失败",example="0")
    @TableField("handle_result")
    private int handleResult;
    /**
     * 执行失败原因
     */
    @ApiModelProperty(value = "执行失败原因",example="xxxxx")
    private String handleMsg;
    /**
     * 执行失败原因中文
     */
    @ApiModelProperty(value = "执行失败原因",example="xxxxx")
    @TableField("handle_msg_zh")
    private String handleMsgZh;
    /**
     * 执行失败原因
     */
    @ApiModelProperty(value = "执行失败原因",example="xxxxx")
    @TableField("handle_msg_en")
    private String handleMsgEn;

    /**
     * 例行工作id
     */
    @ApiModelProperty(value = "例行工作id",example="qwrtgfdssdfgh")
    @TableField("routine_id")
    private String routineId;

    /**
     * 顶级租户ID
     */
    @ApiModelProperty(value = "顶级租户ID",example="110")
    @TableField(value = "top_account_id")
    protected String topAccountId;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID",example="110")
    @TableField(value = "account_id")
    protected String accountId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",example="2021-10-01 01:00:00")
    @TableField("created_time")
    protected Date createdTime = new Date();

    /**
     * 0有效1删除
     */
    @ApiModelProperty(value = "删除标识 0有效1删除",example="0")
    @TableField("is_del")
    protected Integer isDel = 0;

}
