package com.liuwq.job.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author dylan.qin
 * @Since: 2021-07-09 18:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class XxlJobLogParams {
    @ApiModelProperty(value = "xxlJob日志查询参数")
    private int start = 0;
    @ApiModelProperty(value = "xxlJob执行地址")
    private int length = 10000;
    @ApiModelProperty(value = "xxlJob执行器ID")
    private int jobGroup;
    @ApiModelProperty(value = "xxlJob的任务ID")
    private int jobId;
    @ApiModelProperty(value = "xxlJob日志状态")
    private int logStatus = -1;
    @ApiModelProperty(value = "xxlJob的过滤时间")
    private String filterTime;
    /**
     * 详细请求参数
     */
    @ApiModelProperty(value = "xxlJob执行地址")
    private String executorAddress;
    @ApiModelProperty(value = "xxlJob触发时间")
    private long triggerTime;
    @ApiModelProperty(value = "xxlJob的日志ID")
    private int logId;
    @ApiModelProperty(value = "xxlJob执行地")
    private int fromLineNum = 0;
}
