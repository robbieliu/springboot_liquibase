package com.liuwq.job;


import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 *
 *
 * @author wuzhaoyi
 */
@Log4j2
@Service
public class InitAllAccountViewJobHandler {

    @XxlJob("initAllAccountViewJobHandler")
    public void startJob() {
        long start = System.currentTimeMillis();
//        String param = XxlJobHelper.getJobParam();
//        log.info(">>> InitAllAccountViewJobHandler.startJob param:{}", param);

        log.info(">>> InitAllAccountViewJobHandler.startJob end total duration:{}", System.currentTimeMillis() - start);
    }

}
