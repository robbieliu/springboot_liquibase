package com.liuwq.job;


import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * 刷新物化视图定时任务
 *
 * @author wuzhaoyi
 */
@Log4j2
@Service
public class MetarializeViewJobHandlerImpl {

    @XxlJob("metarializeViewJobHandler")
    public void startJob() {
        long start = System.currentTimeMillis();
        log.info(">>> MetarializeViewJobHandlerImpl.startJob timestamp:{}", start);

        log.info(">>> MetarializeViewJobHandlerImpl.startJob end total duration:{}", System.currentTimeMillis() - start);
    }

}
