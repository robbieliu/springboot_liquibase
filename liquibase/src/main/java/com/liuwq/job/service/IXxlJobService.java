package com.liuwq.job.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.liuwq.job.model.*;

import java.util.List;

/**
 * @Author dylan.qin
 * @Since: 2021-07-06 15:28
 */
public interface IXxlJobService {

    /**
     * 注册执行器
     *
     * @param xxlJobGroup
     */
    void registerJobHandler(XxlJobGroup xxlJobGroup) throws IllegalAccessException;

    void updateJonHandler(XxlJobGroup xxlJobGroup)throws IllegalAccessException;

    List<XxlJobGroup> getHandlerList(String appName);

    /**
     * 根据ID删除执行器
     *
     * @param id
     */
    void delJobHandler(int id);

    /**
     * 注册任务
     *
     * @param info
     * @return
     * @throws IllegalAccessException
     */
    int registerJob(XxlJobInfo info) throws IllegalAccessException;

    /**
     * 根据ID删除任务
     *
     * @param id
     */
    void delJob(int id);

    /**
     * 获取所有任务
     *
     * @return
     */
    List<XxlJobInfo> getJobList(int jobGroupId);

    void startJob(int id);

    void stopJob(int id);

    void triggerJob(int id, int jobGroupId);


    void execute(int id, int jobGroupId);

    List<XxlJobLog> getJobLogList(XxlJobLogParams logParams) throws IllegalAccessException;

    IPage<XxlJobLogPo> pageList(int currentPage, int pageSize, String routineId, String accountId);

    String getDetailLog(XxlJobLogParams logParams) throws IllegalAccessException;

    void deleteByIds(String accountId, List<String> idList);

    List<XxlJobInfo> getWorkOrdeJob(int groupId, String handler);

    List<XxlJobInfo> getWorkOrdeJob(int groupId, String handler, String triggerStatus);

    List<XxlJobInfo> getCollGroupChatRecordJob(int groupId, String handler);
    void deleteLogByRoutineId(List<String> idList);

    void delJobBatch(List<Integer> jobIdList);
}
