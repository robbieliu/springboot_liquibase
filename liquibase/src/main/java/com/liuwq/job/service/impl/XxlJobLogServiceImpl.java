package com.liuwq.job.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liuwq.job.model.XxlJobLogPo;
import com.liuwq.job.service.IXxlJobLogService;
import com.liuwq.mapper.XxlJobLogMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author dylan.qin
 * @Since: 2021-07-31 15:27
 */
@Service
@Log4j2
public class XxlJobLogServiceImpl extends ServiceImpl<XxlJobLogMapper, XxlJobLogPo>  implements IXxlJobLogService {
    @Autowired
    XxlJobLogMapper xxlJobLogMapper;

    @Override
    public int insert(XxlJobLogPo xxlJobLogPo) {
        return xxlJobLogMapper.insertLog(xxlJobLogPo);
    }

    @Override
    public void insertBatch(List<XxlJobLogPo> xxlJobLogPoList) {
        if(CollectionUtils.isNotEmpty(xxlJobLogPoList)){
            this.saveBatch(xxlJobLogPoList);
        }
    }

    @Override
    public IPage<XxlJobLogPo> pageList(int currentPage, int pageSize, String routineId) {
        String locale = "zh";
        // 分页查询
        Page<XxlJobLogPo> workPoPage = new Page<>(currentPage, pageSize);
        IPage<XxlJobLogPo> procPoPageList = xxlJobLogMapper.pageList(workPoPage, routineId, locale);
        return procPoPageList;
    }

    @Override
    public void deleteByIds(String accountId, List<String> idList) {
        xxlJobLogMapper.deleteByIds(accountId, idList);
    }


    @Override
    public void deleteByRoutineId( List<String> idList) {
        xxlJobLogMapper.deleteByRoutineId(idList);
    }
}
