package com.liuwq.job.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.liuwq.job.model.XxlJobLogPo;

import java.util.List;

/**
 * @Author dylan.qin
 * @Since: 2021-07-31 15:22
 */
public interface IXxlJobLogService {

    int insert(XxlJobLogPo xxlJobLogPo);

    void insertBatch(List<XxlJobLogPo> xxlJobLogPoList);

    IPage<XxlJobLogPo> pageList(int currentPage,int pageSize,String routineId);

    void deleteByIds(String accountId, List<String> idList);

    void deleteByRoutineId(List<String> idList);
}
