package com.liuwq.job.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.liuwq.config.XxlJobConfig;
import com.liuwq.exception.BaseException;
import com.liuwq.job.model.*;
import com.liuwq.job.service.IXxlJobService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author dylan.qin
 * @Since: 2021-07-06 15:28
 */
@Service
@Slf4j
public class XxlJobServiceImpl implements IXxlJobService {
    @Autowired
    private XxlJobConfig xxlJobConfig;

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private XxlJobLogServiceImpl xXljobLogService;


    @Override
    public void registerJobHandler(XxlJobGroup xxlJobGroup) throws IllegalAccessException {
        log.info("xxl-job operation register job handler ", xxlJobGroup.toString());
        String url = xxlJobConfig.getBaseUrl() + "/jobgroup/save";
        MultiValueMap<String, String> params = objectToMap(xxlJobGroup);

        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            throw new BaseException("register job handler error!");
        }

    }


    @Override
    public List<XxlJobGroup> getHandlerList(String appName) {
        log.info("xxl-job operation getHandlerList,appName:{}", appName);
        List<XxlJobGroup> xxlJobGroupList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/jobgroup/pageList";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("start", "0");
        params.add("length", 10000 + "");
        if (StringUtils.isNotEmpty(appName)) {
            params.add("appname", appName);
        }
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("data") != null) {
            xxlJobGroupList = JSONUtil.toList(JSONUtil.toJsonStr(resultMap.get("data")), XxlJobGroup.class);
            return xxlJobGroupList;
        } else {
            throw new BaseException("select job handler  error!");

        }
    }
    @Override
    public void updateJonHandler(XxlJobGroup xxlJobGroup)throws IllegalAccessException{
        log.info("xxl-job operation update job handler ", xxlJobGroup.toString());
        String url = xxlJobConfig.getBaseUrl() + "/jobgroup/update";
        MultiValueMap<String, String> params = objectToMap(xxlJobGroup);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            throw new BaseException("register job handler error!");
        }

    }

    @Override
    public void delJobHandler(int id) {
        log.info("xxl-job operation delete job handler ,id :{}", id);
        String url = xxlJobConfig.getBaseUrl() + "/jobgroup/remove";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", id + "");
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            throw new BaseException("delete job handler is error!");
        }

    }

    @Override
    public int registerJob(XxlJobInfo info) throws IllegalAccessException {
        log.info("xxl-job operation register job:{}",info.toString());
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/add";
        MultiValueMap<String, String> params = objectToMap(info);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            log.error("register job  error! resultMap:{}",resultMap.toString());
            throw new BaseException("register job  error!");
        } else {
            return Integer.parseInt(resultMap.get("content").toString());
        }
    }

    @Override
    public void delJob(int id) {
        log.info("xxl-job operation delete job handler ,id :{}", id);
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/remove";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", id + "");
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            throw new BaseException("delete job handler is error!");
        }
    }

    @Override
    public List<XxlJobInfo> getJobList(int jobGroupId) {
        log.info("xxl-job operation getJobList ,jobGroupId :{}", jobGroupId);
        List<XxlJobInfo> xxlJobGroupList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/pageList";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("start", "0");
        params.add("length", 10000 + "");
        params.add("jobGroup", jobGroupId + "");
        params.add("triggerStatus", "-1");
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("data") != null) {
            xxlJobGroupList = JSONUtil.toList(JSONUtil.toJsonStr(resultMap.get("data")), XxlJobInfo.class);
            return xxlJobGroupList;
        } else {
            throw new BaseException("select job handler  error!");

        }
    }

    @Override
    public void startJob(int id) {
        log.info("xxl-job operation start job ,id :{}", id);
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/start";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", id + "");
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            log.debug("resultMap:{}", resultMap);
            throw new BaseException("start job is error!");
        }

    }

    @Override
    public void stopJob(int id) {
        log.info("xxl-job operation stop job  ,id :{}", id);
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/stop";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", id + "");
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            throw new BaseException("stop job  is error!");
        }

    }

    @Override
    public void triggerJob(int id, int jobGroupId) {
        try {
            startJob(id);
        } catch (Exception e) {
            execute(id, jobGroupId);
        }

    }

    @Override
    public void execute(int id, int jobGroupId) {
        List<XxlJobInfo> list = getJobList(jobGroupId);
        XxlJobInfo xxlJobInfo = null;
        for(XxlJobInfo info : list) {
            if(info.getId() == id) {
                xxlJobInfo = info;
                break;
            }
        }
        if(xxlJobInfo == null) {
            throw new BaseException("job id : "+id+" not exists in jobGroup id : "+jobGroupId+" !");
        }
        String param = xxlJobInfo.getExecutorParam();

        log.info("xxl-job operation execute one time job ,id :{}", id);
        String url = xxlJobConfig.getBaseUrl()+"/jobinfo/trigger";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("id", id+"");
        params.add("executorParam", param);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
            log.debug("resultMap:{}", resultMap);
            throw new BaseException("execute job is error!");
        }

    }


    @Override
    public List<XxlJobLog> getJobLogList(XxlJobLogParams logParams) throws IllegalAccessException {
        log.info("xxl-job operation getJobLogList ,logParams :{}", logParams);
        List<XxlJobLog> xxlJoblogList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/joblog/pageList";
        MultiValueMap<String, String> params = objectToMap(logParams);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("data") != null) {
            xxlJoblogList = JSONUtil.toList(JSONUtil.toJsonStr(resultMap.get("data")), XxlJobLog.class);
            return xxlJoblogList;
        } else {
            throw new BaseException("select job handler  error!");

        }
    }
    @Override
   public  IPage<XxlJobLogPo> pageList(int currentPage, int pageSize, String routineId, String accountId){
       return xXljobLogService.pageList(currentPage, pageSize, routineId);
    }


    @Override
    public String getDetailLog(XxlJobLogParams logParams) throws IllegalAccessException {
        log.info("xxl-job operation getDetailLog ,logParams :{}", logParams);
        List<XxlJobLog> xxlJoblogList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/joblog/logDetailCat";
        MultiValueMap<String, String> params = objectToMap(logParams);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("content") != null) {
            Map<String, Object> contentMap = JSONUtil.parseObj(JSONUtil.toJsonStr(resultMap.get("content")));
            return contentMap.get("logContent").toString();
        } else {
            throw new BaseException("select job handler  error!");

        }
    }


    /**
     * 将Object对象里面的属性和值转化成Map对象
     *
     * @param obj
     * @return
     * @throws IllegalAccessException
     */
    public static MultiValueMap<String, String> objectToMap(Object obj) throws IllegalAccessException {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value = field.get(obj);
            if (value != null) {
                map.add(fieldName, value.toString());
            }
        }
        return map;
    }


    private HttpHeaders getRequestHeader() {
        log.info("xxl-job operation getRequestHeader");
        String url = xxlJobConfig.getBaseUrl() + "/login";
        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("userName", xxlJobConfig.getUserName());
        params.add("password", xxlJobConfig.getPassword());

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);

        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) == 200) {
            List<String> cookieList = resp.getHeaders().get(HttpHeaders.SET_COOKIE);
            requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            requestHeaders.addAll(HttpHeaders.COOKIE, cookieList);
            return requestHeaders;
        } else {
            throw new BaseException("xxlJob login error");
        }
    }
    @Override
    public void deleteByIds(String accountId, List<String> idList){
        xXljobLogService.deleteByIds(accountId, idList);
    }
    @Override
    public void deleteLogByRoutineId( List<String> idList){
        xXljobLogService.deleteByRoutineId(idList);
    }

    @Override
    public List<XxlJobInfo> getWorkOrdeJob(int groupId ,String handler){
        return getWorkOrdeJob(groupId, handler, "1");
    }

    @Override
    public List<XxlJobInfo> getWorkOrdeJob(int groupId ,String handler, String triggerStatus){
        log.info("xxl-job operation getWorkOrdeJob,groupId:{},handler:{},triggerStatus:{}",groupId,handler,triggerStatus);
        List<XxlJobInfo> xxlJobGroupList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/pageList";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("start", "0");
        params.add("length", 10 + "");
        params.add("jobGroup", groupId + "");
        params.add("triggerStatus", triggerStatus);
        params.add("executorHandler",handler);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("data") != null) {
            xxlJobGroupList = JSONUtil.toList(JSONUtil.toJsonStr(resultMap.get("data")), XxlJobInfo.class);
            return xxlJobGroupList;
        } else {
            throw new BaseException("select job handler  error!");
        }
    }

    @Override
    public List<XxlJobInfo> getCollGroupChatRecordJob(int groupId, String handler) {
        log.info("xxl-job operation getCollGroupChatRecordJob,groupId:{},handler:{}",groupId,handler);
        List<XxlJobInfo> xxlJobGroupList = new ArrayList<>();
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/pageList";
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("start", "0");
        params.add("length", 10 + "");
        params.add("jobGroup", groupId + "");
        params.add("triggerStatus", "1");
        params.add("executorHandler",handler);
        HttpHeaders requestHeaders = getRequestHeader();
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
        ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
        String result = resp.getBody();
        Map<String, Object> resultMap = JSONUtil.parseObj(result);
        if (resultMap.get("data") != null) {
            xxlJobGroupList = JSONUtil.toList(JSONUtil.toJsonStr(resultMap.get("data")), XxlJobInfo.class);
            return xxlJobGroupList;
        } else {
            throw new BaseException("select job handler  error!");
        }
    }

    @Override
    public void delJobBatch(List<Integer> idList) {
        log.info("xxl-job operation delete batch job handler ,id :{}", idList);
        HttpHeaders requestHeaders = getRequestHeader();
        String url = xxlJobConfig.getBaseUrl() + "/jobinfo/remove";

        idList.stream().forEach(id -> {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("id", id + "");

            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, requestHeaders);
            log.info("xxl-job operation delete batch job start ,id :{}", id);
            ResponseEntity<String> resp = restTemplate.postForEntity(url, requestEntity, String.class);
            String result = resp.getBody();
            log.info("xxl-job operation delete batch job end ,result :{}", result);
            Map<String, Object> resultMap = JSONUtil.parseObj(result);
            if (resultMap.get("code") != null && Integer.parseInt(resultMap.get("code").toString()) != 200) {
                throw new BaseException("delete job handler error,id is " + id);
            }
        });
    }
}
