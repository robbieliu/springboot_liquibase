package com.liuwq.job.service.impl;

import com.liuwq.config.XxlJobConfig;
import com.liuwq.job.model.XxlJobGroup;
import com.liuwq.job.model.XxlJobInfo;
import com.liuwq.job.service.IXxlJobService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @Author dylan.qin
 * @Since: 2021-09-01 16:18
 */
@Service
@Log4j2
public class XxlJobInitService {
    @Autowired
    IXxlJobService xxlJobService;
    @Autowired
    XxlJobConfig xxlJobConfig;

    public static final String TITLE = "DOSM";
    public static final String AUTHOR = "liuwq";

    public static final String METARIALIZE_VIEW_JOB_HANDLER = "metarializeViewJobHandler";
    public static final String INIT_ALL_ACCOUNT_VIEW_JOB_HANDLER = "initAllAccountViewJobHandler";

    @Value("${xxl.job.materialize.view.cron:0/5 * * * * ?}")
    private String materializeViewCron;

    @Value("${xxl.job.init.account.view.cron:0/1 * * * * ?}")
    private String initAllAccountViewCron;


    @PostConstruct
    public void init() {
        String appName = xxlJobConfig.getAppName();
        try {
            XxlJobGroup xxlJobGroup = getXxlJobGroupByName(appName);
            registerMetarializeViewJob(xxlJobGroup);
            registerInitAllAccountViewJobHandler(xxlJobGroup);
        } catch (Exception e) {
            log.error("message notify register job error!", e);
        }

    }

    private XxlJobGroup getXxlJobGroupByName(String appName) throws IllegalAccessException {
        // 查询是否需要注册JobHandler
        List<XxlJobGroup> jobGroupList = xxlJobService.getHandlerList(appName);
        if (jobGroupList.isEmpty()) {
            // 注册xxlJobHelder
            XxlJobGroup xxlJobGroup = new XxlJobGroup();
            xxlJobGroup.setAppname(appName);
            xxlJobGroup.setTitle(TITLE);
            xxlJobService.registerJobHandler(xxlJobGroup);
            jobGroupList = xxlJobService.getHandlerList(appName);
        } else {
            xxlJobService.updateJonHandler(jobGroupList.get(0));
        }
        return jobGroupList.get(0);
    }


    private void registerMetarializeViewJob(XxlJobGroup xxlJobGroup) throws IllegalAccessException {

        List<XxlJobInfo> materializeViewList = xxlJobService.getWorkOrdeJob(xxlJobGroup.getId(), METARIALIZE_VIEW_JOB_HANDLER);
        if (!CollectionUtils.isEmpty(materializeViewList)) {
            log.warn("metarializeViewJobHandler 已经注册，无需重复注册");
            return;
        }

        // 查询是否需要注册JobHandler
        XxlJobInfo info = XxlJobInfo.builder()
                .jobGroup(xxlJobGroup.getId())
                .jobDesc("定时同步物化视图数据")
                .author(AUTHOR)
                .scheduleConf(materializeViewCron)
                .executorHandler(METARIALIZE_VIEW_JOB_HANDLER)
                .executorTimeout(0)
                .executorFailRetryCount(0)
                .build();
        info.setDefault();
        log.info("register materialize view   delay job:{}", info.toString());
        int jobId = xxlJobService.registerJob(info);
        // 启动xxlJob
        xxlJobService.startJob(jobId);
        log.info("开始物化视图定时任务 jobId = {}", jobId);

    }


    private void registerInitAllAccountViewJobHandler(XxlJobGroup xxlJobGroup) throws IllegalAccessException {
        // 查询是否需要注册JobHandler
        List<XxlJobInfo> xxlJobInfos = xxlJobService.getWorkOrdeJob(xxlJobGroup.getId(), INIT_ALL_ACCOUNT_VIEW_JOB_HANDLER, "0");
        if(CollectionUtils.isEmpty(xxlJobInfos)){
            xxlJobInfos = xxlJobService.getWorkOrdeJob(xxlJobGroup.getId(), INIT_ALL_ACCOUNT_VIEW_JOB_HANDLER);
        }

        if(!CollectionUtils.isEmpty(xxlJobInfos)) {
            log.warn("registerInitAllAccountViewJobHandler 已经注册，无需重复注册");
            return;
        }

        // 注册job
        XxlJobInfo info = XxlJobInfo.builder()
                .jobGroup(xxlJobGroup.getId())
                .jobDesc("定时全量租户初始化物化视图数据")
                .author(AUTHOR)
                .scheduleConf(initAllAccountViewCron)
                .executorHandler(INIT_ALL_ACCOUNT_VIEW_JOB_HANDLER)
                .executorTimeout(0)
                .executorFailRetryCount(0)
                .triggerStatus(0)
                .build();
        info.setDefault();
        log.info("registerInitAllAccountViewJobHandler register initAllAccountViewJobHandler   delay job:{}", info.toString());
        Integer jobId = xxlJobService.registerJob(info);

        // 默认不启动xxlJob
//          xxlJobService.startJob(jobId);
        log.info("registerInitAllAccountViewJobHandler register jobId = {}", jobId);

    }
    
}

