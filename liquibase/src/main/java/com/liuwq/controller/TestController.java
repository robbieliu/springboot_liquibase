package com.liuwq.controller;

import com.liuwq.service.TestService;
import com.liuwq.strategy.NotificationWayManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@Slf4j
public class TestController {

    @Autowired
    private NotificationWayManager notificationWayManager;

    @Autowired
    private TestService testService;

    /**
     * http://localhost:8082/test?notifyWay=EMAIL
     * http://localhost:8082/test?notifyWay=SMS
     * @param notifyWay
     * @return
     */
    @GetMapping("/test")
    public String test(@RequestParam(required = false, defaultValue = "SMS") String notifyWay) {
        log.info(" ------ 1 -------");
        for (int i = 0; i < 5; i++) {
            int random = new Random().nextInt(10000);
            testService.syncTest(i, random);
        }
        log.info(" ------ 2 -------");
        return notificationWayManager.getNotifierInfo(notifyWay).doBusiness();
    }

}
