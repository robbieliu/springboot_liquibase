package com.liuwq.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * xxlJob 配置类
 *
 * @Author dylan.qin
 * @Since: 2021-07-06 15:34
 */
@Configuration
@Data
public class XxlJobConfig {

    @Value("${xxl.job.admin.addresses:http://127.0.0.1:8080/xxl-job-admin/}")
    private String baseUrl;

    @Value("${xxl.job.user.name:admin}")
    private String userName;

    @Value("${xxl.job.user.password:123456}")
    private String password;

    @Value("${xxl.job.executor.appname:dosm3}")
    private String appName;

}
