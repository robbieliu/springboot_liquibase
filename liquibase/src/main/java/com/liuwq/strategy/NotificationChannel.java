package com.liuwq.strategy;

public interface NotificationChannel {

    String notifyWay();

    String doBusiness();

}
