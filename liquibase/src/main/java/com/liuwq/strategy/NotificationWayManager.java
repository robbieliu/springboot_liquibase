package com.liuwq.strategy;

import com.google.common.collect.Maps;
import com.liuwq.exception.BaseException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentMap;


/**
 * All of notification way
 *
 * @author <a href="mailto:domi.song@cloudwise.com">domisong</a>
 * @since 2021/6/24
 */
@Log4j2
@Component
public class NotificationWayManager implements BeanPostProcessor {

    public final ConcurrentMap<String, NotificationChannel> notifierInfoMap = Maps.newConcurrentMap();

    /**
     * 返回通知方式相关的信息
     *
     * @param notifyWay 通知方式
     */
    public NotificationChannel getNotifierInfo(String notifyWay) {
        if (!notifierInfoMap.containsKey(notifyWay)) {
            throw new BaseException(String.format("NOT SUPPORT notification way. %s", notifyWay));
        }
        return notifierInfoMap.get(notifyWay);
    }

//    @Override
//    public Object postProcessBeforeInitialization(@NonNull Object bean, @NonNull String beanName) throws BeansException {
//        if (!(bean instanceof NotificationChannel)) {
//            return bean;
//        }
//
//        NotificationChannel notifierInfo = (NotificationChannel) bean;
//        String notifyWay = notifierInfo.notifyWay();
//        NotificationChannel realNotificationInfo = notifierInfoMap.putIfAbsent(notifyWay, notifierInfo);
//
//        if (Objects.nonNull(realNotificationInfo)) {
//            throw new BaseException(String.format("duplicate notifier info. %s", notifyWay));
//        }
//        return null;
//    }


    @Autowired(required = false)
    public void setActionTypeSelector(List<NotificationChannel> selectors) {
        if (CollectionUtils.isNotEmpty(selectors)) {
            for (NotificationChannel selector : selectors) {
                log.info("ActionTypeSelector初始化 ===================，type:{}", selector.notifyWay());
                String type = selector.notifyWay();
                NotificationChannel actionTypeSelector = notifierInfoMap.putIfAbsent(type, selector);

                if (Objects.nonNull(actionTypeSelector)) {
                    throw new BaseException(String.format("duplicate notification type. %s", type));
                }
            }
        }
    }

}
