package com.liuwq.strategy.service;

import com.liuwq.strategy.NotificationChannel;
import com.liuwq.strategy.NotifyWay;
import org.springframework.stereotype.Component;

/**
 * 用户手机号处理类
 *
 * @author <a href="mailto:domi.song@cloudwise.com">domisong</a>
 * @since 2021/6/29
 */
@Component
public class SmsService implements NotificationChannel {

    @Override
    public String notifyWay() {
        return NotifyWay.SMS.name();
    }

    @Override
    public String doBusiness() {
        return "SmsService";
    }
}
