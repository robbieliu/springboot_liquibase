package com.liuwq.strategy;


import com.google.common.base.Enums;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * 通知渠道
 *
 * @author kensou
 * */
public enum NotifyWay {

    WEB,

    IM,

    /** 邮箱 */
    EMAIL,

    /** 短信 */
    SMS,

    NAIL,

    /** 系统 */
    SYSTEM,

    /** 企业微信 */
    WE_CHAT,

    /** 钉钉 */
    DING_TALK,

    /** 飞书 */
    FEI_SHU,

    TEAM_TALK,

    /** RocketMQ */
    MQ,
    /**
     * 微信推送
     */
    WX_PUSH,
    /**
     * 工单协作群
     */
    ORDER_GROUP,
    /**
     * 其他来源
     */
    OTHERS
    ;

    public static NotifyWay getIfPresent(String name) {
        return Enums.getIfPresent(NotifyWay.class, name).or(OTHERS);
    }


    public static Optional<NotifyWay> ofNullable(String notifyWay) {
        return Stream.of(values())
                .filter(bean -> Objects.equals(notifyWay, bean.name()))
                .findAny();
    }
}
