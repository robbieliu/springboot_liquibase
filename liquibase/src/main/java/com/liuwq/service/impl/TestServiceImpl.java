package com.liuwq.service.impl;

import com.liuwq.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    private ThreadPoolTaskExecutor syncExecutorPool;

    @Override
    @Async("syncExecutorPool")
    public void syncTest(int i, int random) {
        syncExecutorPool.execute(() -> {
            try {
                Thread.sleep(random);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("{} --- {} --- 随机睡眠:{}毫秒", Thread.currentThread().getName(), i, random);
        });// 需要传递Runnable对象

        log.info(" ------ 3 -------");
    }

}
