package com.liuwq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liuwq.job.model.XxlJobLogPo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author dylan.qin
 * @Since: 2021-07-31 14:56
 */
@Mapper
@Repository
public interface XxlJobLogMapper extends BaseMapper<XxlJobLogPo> {

    /**
     * 获取xxlJobLog列表
     *
     * @param page
     * @param routineId
     * @return
     */
    IPage<XxlJobLogPo> pageList(Page<?> page, String routineId,String locale);

    /**
     * 删除日志信息
     *
     * @param accountId
     * @param idList
     */
    void deleteByIds(String accountId, List<String> idList);

    /**
     *
     * @param idList
     */
    void deleteByRoutineId( List<String> idList);

    int insertLog (XxlJobLogPo xxlJobLogPo);

    void handleAnalyze();
}
