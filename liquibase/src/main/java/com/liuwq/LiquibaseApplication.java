package com.liuwq;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class LiquibaseApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(LiquibaseApplication.class).run(args);
    }
}