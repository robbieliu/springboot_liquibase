package com.liuwq.exception;

import lombok.Getter;

import java.io.Serializable;

/**
 * @author admin
 */
@Getter
public class BaseException extends RuntimeException implements Serializable {

    public static final long serialVersionUID = 1L;
    private final boolean debug = true;
    private final String trace = null;
    private transient Object[] args;
    private int code = 100001;
    private Object data;
    
    private String message = "";

    public BaseException() {
    	super();
    }

    public BaseException(String msg) {
        super();
        this.message = msg;
        ExceptionHolder.setExceptionCode(this);
    }
    public BaseException(int code) {
    	super();
        this.code = code;
        ExceptionHolder.setExceptionCode(this);
    }
    
    public BaseException(int code, String message) {
    	super();
        this.code = code;
        this.message = message;
        ExceptionHolder.setExceptionCode(this);
    }

    public BaseException(int code, Throwable cause) {
        super(cause);
        this.code = code;
        ExceptionHolder.setExceptionCode(this);
    }

    public BaseException(int code, Object errorInfo) {
        super();
        this.code = code;
        this.data = errorInfo;
        ExceptionHolder.setExceptionCode(this);
    }

    public BaseException(int code,String message,Object errorInfo){
        this(code,message);
        this.data = errorInfo;
    }

    public BaseException(int code, Object[] args) {
    	super();
        this.code = code;
        this.args = args;
        ExceptionHolder.setExceptionCode(this);
    }

    public BaseException(Throwable cause, int code) {
        super(cause);
        this.code = code;
        ExceptionHolder.setExceptionCode(this);
    }

    public BaseException(String msg, Exception cause) {
        super(cause);
        this.message = msg;
        ExceptionHolder.setExceptionCode(this);
    }

    @Override
    public String getMessage() {
    	return this.message;
    }


    public void setArgs(Object[] args) {
        this.args = args;
    }
}