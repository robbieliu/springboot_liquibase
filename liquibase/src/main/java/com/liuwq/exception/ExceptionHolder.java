package com.liuwq.exception;

/**
 * <p>
 *
 * </p>
 *
 * @author Norval.Xu
 * @since 2023/6/1
 */
public class ExceptionHolder {
    public static ThreadLocal<BaseException> EXCEPTION_CODE_TREAD_LOCAL = new ThreadLocal<>();

    public static  void setExceptionCode(BaseException code){
        EXCEPTION_CODE_TREAD_LOCAL.set(code);
    }

    public static BaseException getExceptionCode(){
        return EXCEPTION_CODE_TREAD_LOCAL.get();
    }

    public static void remove() {
        EXCEPTION_CODE_TREAD_LOCAL.remove();
    }
}
