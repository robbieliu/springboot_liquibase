#!/bin/bash
PWDPATH=`dirname $0`
PORTAL_HOME=`cd $PWDPATH && cd .. && pwd`
echo $PORTAL_HOME
cd $PORTAL_HOME
JVM_OPTS="
-server
 -Xms512m
 -Xmx512m
 -XX:+UseG1GC
 -XX:MaxGCPauseMillis=200
 -XX:+DisableExplicitGC
 -XX:+PrintGCDetails
 -XX:-UseGCOverheadLimit
 -XX:+HeapDumpOnOutOfMemoryError
 -XX:+PrintGCDateStamps
 -Xloggc:logs/gc.log
 -XX:+HeapDumpOnOutOfMemoryError
 -XX:HeapDumpPath=logs/heap.hprof
"

start() {
nohup java $JVM_OPTS -Dlogging.config=$PORTAL_HOME/config/log4j2-spring.xml -jar $PORTAL_HOME/lib/liquibase-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &
echo -e '\r'
}
logs_dir=$PORTAL_HOME/logs
if [ ! -d "$logs_dir" ]; then
        mkdir $logs_dir
fi
start
